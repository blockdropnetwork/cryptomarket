"use strict";

document.addEventListener('DOMContentLoaded', function() {




//start spr-countdown
var $counter_4col_timer_countdown__0 = $('#counter-4col-timer-countdown--0');
$counter_4col_timer_countdown__0.countdown('2019/03/02 23:59:59', function (event) {
    $counter_4col_timer_countdown__0.find('.days').html(event.strftime('%D'));
    $counter_4col_timer_countdown__0.find('.hours').html(event.strftime('%H'));
    $counter_4col_timer_countdown__0.find('.minutes').html(event.strftime('%M'));
    $counter_4col_timer_countdown__0.find('.seconds').html(event.strftime('%S'));
}).on('finish.countdown', function () {
}//end finish.countdown
);//end spr-countdown

});
